provider "vscale" {
  token = "${var.token_vscale}"
}

resource "random_string" "password" {
  count            = "${length(var.devs)}"
  length           = 12
  special          = true
  override_special = "#$%"
}

resource "vscale_scalet" "ilyascalerb" {
  count     = "${length(var.devs)}"
  location  = "spb0"
  make_from = "debian_8_64_001_master"
  name      = "ilya-nginx-${count.index}"
  rplan     = "medium"
  ssh_keys  = ["${var.key_id}"]

  provisioner "remote-exec" {
    inline = [
      "echo root:${random_string.password.result} | chpasswd",
    ]
  }
}

output "ip" {
  value = ["${vscale_scalet.ilyascalerb.*.public_address}"]
}

output "pwd" {
  value = ["${random_string.password.result}"]
}
