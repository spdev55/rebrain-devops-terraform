provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

data "aws_route53_zone" "selid" {
  name = "devops.rebrain.srwx.net"
}

resource "aws_route53_record" "g_dogg" {
  count   = "${length(var.devs)}"
  zone_id = "${data.aws_route53_zone.selid.zone_id}"
  name    = "g-dogg-${count.index}.${data.aws_route53_zone.selid.name}"
  type    = "A"
  ttl     = "300"
  records = ["${element(vscale_scalet.ilyascalerb.*.public_address, count.index)}"]
}

output "dns" {
  value = ["${aws_route53_record.g_dogg.*.name}"]
}
